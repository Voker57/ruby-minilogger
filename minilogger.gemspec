Gem::Specification.new do |s|
  s.name        = 'minilogger'
  s.version     = '0.0.2'
  s.date        = '2019-05-18'
  s.summary     = "minimalistic logging library"
  s.description = ""
  s.authors     = ["Voker57"]
  s.email       = 'voker57@gmail.com'
  s.files       = Dir['lib/*.rb']
  s.homepage    =
    'https://gitlab.com/Voker57/ruby-minilogger'
  s.license       = 'MIT'
end
