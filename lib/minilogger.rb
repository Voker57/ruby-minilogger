class Minilogger
	@level_hash = {
		emerg: 0,
		alert: 1,
		crit: 2,
		err: 3,
		warning: 4,
		notice: 5,
		info: 6,
		debug: 7
		}
	
	attr_reader :log_level
	
	# Create a new Minilogger object with log level determined from LOG_LEVEL environment variable
	def self.from_env
		if ENV["LOG_LEVEL"]
			Minilogger.new(ENV["LOG_LEVEL"])
		else
			Minilogger.new
		end
	end
	
	def initialize(level = :warning)
		self.log_level = level
	end
	
	def self.level_to_int(level)
		int_level = @level_hash[level.to_sym]
		if !int_level
			raise "Undefined log level: #{level}"
		end
		return int_level
	end
	
	def log_level=(level)
		@log_level = Minilogger.level_to_int(level)
	end
	
	def log(message, level = :info)
		if log_level >= Minilogger.level_to_int(level)
			if message.is_a? String
				message = message.split("\n")
			end
			message.each do |mpart|
				STDERR.puts "#{level}: #{mpart}"
			end
			STDERR.flush
		end
		return
	end
	
	def emerg(message)
		log(message, :emerg)
	end
	
	def alert(message)
		log(message, :alert)
	end
	
	def crit(message)
		log(message, :crit)
	end
	
	def err(message)
		log(message, :err)
	end
	
	def error(message)
		log(message, :err)
	end
	
	def warning(message)
		log(message, :warning)
	end
	
	def warn(message)
		log(message, :warning)
	end
	
	def notice(message)
		log(message, :notice)
	end
	
	def info(message)
		log(message, :info)
	end
	
	def debug(message)
		log(message, :debug)
	end
end
